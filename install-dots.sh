#!/usr/bin/env bash

echo "Installing my dotfiles..."
git clone --bare https://gitlab.com/KSatheeskumar21/dotfiles.git $HOME/.cfg
function config {
    /usr/bin/git --git-dir=$HOME/.cfg  --work-tree=$HOME $@
}

# Installing Paru
echo -e "Installing Paru and Aura..."
sudo pacman -U .bin/*.pkg.tar.zst

# Installing required programs
paru -Syu \
	exa starship alacritty fish zsh bash qutebrowser nomacs rofi \
       	emacs ripgrep fd vim neovim \
	lxappearance pcmanfm nitrogen picom \
	spectrwm \
	powerline powerline-fonts python-powerline powerline-common \
	cmake rust cargo shellcheck \
	ttf-jetbrains-mono ttf-joypixels nerd-fonts-complete \
	xmonad xorg xorg-xinit xorg-xinput picom-jonaburg-git acpi candy-icons-git wmctl playerctl xmonad-contrib dunst jq xclip maim rofi-greenclip xautolock betterlockscreen

echo "Installing AUR packages..."
paru -S pokemon-colorscripts-git vscodium-bin leftwm leftwm-theme-git marked python-nose-timer python-isort python-pipenv racer nerd-fonts-complete
echo "Required programs installed..., if you want my suckless builds, go to https://gitlab.com/KSatheeskumar21/suckless-tools"

cd $HOME
mkdir -p .config-backup
config checkout
if [ $? = 0 ]; then
    echo "Checked out config"
    else
        echo "Backing up pre-existing dots";
        config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}
fi;
config checkout
config config status.showUntrackedFiles no

echo "Installing Doom Emacs..."

git clone https://github.com/hlissner/doom-emacs ~/.emacs.d

echo "Installing..."
~/.emacs.d/bin/doom install

# echo "Installing Oh My Zsh..."
# sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

