#!/usr/bin/env bash

echo "Starting programs" &
lxsession &
picom &
/usr/bin/emacs --daemon &
volumeicon &
nm-applet &

nitrogen --restore &

echo "Done" &