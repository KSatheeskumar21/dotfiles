set nocompatible
set splitbelow splitright
set number

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

call plug#begin('~/.vim/plugged')

     Plug 'ghifarit53/tokyonight-vim'

call plug#end()


set termguicolors

let g:tokyonight_style = 'night' 
let g:tokyonight_enable_italic = 1

colorscheme tokyonight
